/* IMPORTS */
import { useState, useEffect } from "react";
//react-router-dom
import { BrowserRouter as Router } from "react-router-dom";
import { Routes, Route } from "react-router-dom";

//pages
import Home from "./pages/Home";
import Login from "./pages/Login";
import Supplies from "./pages/Supplies";
import ItemView from "./components/ItemView";
import MyOrder from "./pages/MyOrder";
import Register from "./pages/Register";
import Dashboard from "./pages/Dashboard";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
//components
import Navbar from "./components/common/Navbar";
import { UserProvider } from "./global/UserContex";

import image from "./assets/images/prontera.jpg";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log("Current user: " + user.id);
    console.log(localStorage);
  }, [user]);
  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/myorders" element={<MyOrder />} />
          <Route path="/supplies" element={<Supplies />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/items/:itemId" element={<ItemView />} />
          <Route path="/register" element={<Register />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="*" element={<Error />} />
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
