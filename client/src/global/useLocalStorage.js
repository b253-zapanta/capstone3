export const getItemFromStorage = (key) => {
  const item = window.localStorage.getItem(key);
  return item ? JSON.stringify(item) : null;
};

export const setItemInStorage = (name, data) => {
  window.localStorage.setItem(name, JSON.stringify(data));
};

export const removeItemFromStorage = (name) => {
  console.log(`removed ${name}`);
  window.localStorage.removeItem(name);
};
