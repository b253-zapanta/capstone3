import { ListGroup, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function ItemCard({ item }) {
  const { itemName, quantity, price } = item;

  return (
    <div>
      <span className="text-secondary"> ${price} </span>
      <span className="info">{itemName}</span>
      <span className="text-success quantity"> {quantity}x</span>
      <hr></hr>
    </div>
  );
}
