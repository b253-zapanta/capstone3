import { Button, Row, Col } from "react-bootstrap";
export default function Banner() {
  return (
    <Row>
      <Col className="p-5">
        <h1>Oligarchy</h1>
        <p>4x33</p>
        <Button variant="dark" size="sm">
          About
        </Button>
      </Col>
    </Row>
  );
}
