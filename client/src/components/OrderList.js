import { useEffect, useState } from "react";
import { ListGroup, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function OrderList(order) {
  const options = {
    month: "long",
    day: "numeric",
    year: "numeric",
  };

  const date = new Date(order.item.orderedOn);
  const formattedDate = date.toLocaleDateString("en-US", options);

  return (
    <div>
      <span className="text-secondary">{formattedDate} </span>
      <span className="info"> Total amount : {order.item.totalAmount}</span>
      <ul>
        {order.item.items.map((item, index) => (
          <li key={index}>
            Item Name: {item.itemName}, Quantity: {item.quantity}, Price:
            {item.price}
          </li>
        ))}
      </ul>
      <hr></hr>
    </div>
  );
}
