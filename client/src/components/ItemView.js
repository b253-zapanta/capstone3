import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useParams, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../global/UserContex";
import "../assets/css/index.css";

//env
const renderUrl = process.env.REACT_APP_RENDER_URL;
const localUrl = process.env.REACT_APP_LOCAL_URL;
const mode = process.env.REACT_APP_MODE;
const link = mode === "DEV" ? localUrl : renderUrl;

export default function ItemView() {
  const { itemId } = useParams();
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  let token = localStorage.getItem("token");

  const [name, setName] = useState("");
  const [quantity, setQuantity] = useState("");
  const [category, setCategory] = useState("");
  const [stock, setStock] = useState(0);
  const [price, setPrice] = useState(0);
  let isAvail = stock !== "0";

  const [refresh, setRefresh] = useState(0);

  function refreshData() {
    setRefresh((x) => x + 1);
  }

  function addToCart(itemId) {
    fetch(`${link}/api/supply/${itemId}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          saveToCartDB(data);
        } else {
          console.log("error no data from fetch get item details");
        }
      });
  }

  function saveToCartDB(data) {
    fetch(`${link}/api/addToCart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        itemId: data._id,
        quantity: quantity,
        itemName: data.name,
        price: data.price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          // console.log(data.data);
          if (data.status === "success") {
            Swal.fire({
              title: "Success",
              icon: "success",
              text: "Requested Item recorded.",
            });
            navigate("/supplies");
          } else {
            Swal.fire({
              title: "Something went wrong",
              icon: "error",
              text: "Please try again.",
            });
          }
        } else {
          console.log("saving to cart db failed");
        }
      });
  }

  useEffect(() => {
    fetch(`${link}/api/supply/${itemId}`)
      .then((res) => res.json())
      .then((data) => {
        setName(data.name);
        setCategory(data.category);
        setStock(data.stock);
        setPrice(data.price);
      });
  }, [itemId, refresh]);

  return (
    <div className="bg">
      <div className="overlayBg">
        <Container className="p-5">
          <Row>
            <Col>
              <Card className="my-3">
                <Card.Body>
                  <Card.Title>{name}</Card.Title>
                  <Card.Subtitle>Category:</Card.Subtitle>
                  <Card.Text>{category}</Card.Text>
                  <Card.Subtitle>Price:</Card.Subtitle>
                  <Card.Text>{price}</Card.Text>
                  <Card.Subtitle>Stock:</Card.Subtitle>
                  <Card.Text>{stock}</Card.Text>
                  <Card.Subtitle>Quantity:</Card.Subtitle>
                  <Card.Text className="mt-1 mb-3">
                    <input
                      type="number"
                      step="any"
                      min="0"
                      max="500"
                      size="sm"
                      value={quantity}
                      onChange={(e) => {
                        e.target.value < stock
                          ? setQuantity(e.target.value)
                          : setQuantity(stock);
                      }}
                    />
                  </Card.Text>
                  {isAvail ? (
                    <>
                      <Button
                        className="me-1"
                        variant="dark"
                        size="sm"
                        block
                        onClick={() => addToCart(itemId)}
                      >
                        Add to cart
                      </Button>
                      <Button
                        size="sm"
                        className="btn btn-warning btn-block mx-1"
                        onClick={() => navigate("/supplies")}
                      >
                        Back
                      </Button>
                    </>
                  ) : (
                    <>
                      <Button
                        size="sm"
                        className="btn btn-secondary btn-block me-1"
                        disabled
                      >
                        No stock
                      </Button>

                      <Button
                        size="sm"
                        className="btn btn-dark btn-block mx-1"
                        onClick={() => navigate("/supplies")}
                      >
                        Back
                      </Button>
                    </>
                  )}
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}
