import { Card, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

import "../assets/images/prontera.jpg";

export default function ItemCard({ item }) {
  const { _id, name, stock, price } = item;

  return (
    <Col>
      <Card className="my-3 supply-cards" style={{ width: "20rem" }}>
        <Card.Img
          variant="top"
          src="https://mcdn.wallpapersafari.com/medium/95/92/IPCE5c.jpg"
        />
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Text className="text-muted">
            Some quick example text to build on the card title and make up the
            bulk of the card's content
          </Card.Text>
          <Row className="p-2">
            <Col md={6}>
              <Card.Subtitle className="text-primary">Stock:</Card.Subtitle>
              <Card.Text>{stock}</Card.Text>
            </Col>
            <Col md={6}>
              <Card.Subtitle className="text-primary">Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>
            </Col>
          </Row>

          <Link
            className="btn btn-warning text-light my-2"
            size="sm"
            to={`/items/${_id}`}
          >
            View Item
          </Link>
        </Card.Body>
      </Card>
    </Col>
  );
}
