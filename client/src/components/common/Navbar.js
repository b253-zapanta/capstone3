import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { useContext } from "react";

import { Link, NavLink } from "react-router-dom";

import UserContext from "../../global/UserContex";

import React, { useState } from "react";

function MainNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container fluid>
        <Navbar.Brand href="/">4x33</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/">
              Home
            </Nav.Link>

            {user.id !== null ? (
              <>
                {user.isAdmin ? (
                  <>
                    <Nav.Link as={NavLink} to="/dashboard">
                      Dashboard
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/logout">
                      Logout
                    </Nav.Link>
                  </>
                ) : (
                  <>
                    <Nav.Link as={NavLink} to="/supplies">
                      Market
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/myorders">
                      Orders
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/logout">
                      Logout
                    </Nav.Link>
                  </>
                )}
              </>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default MainNavbar;
