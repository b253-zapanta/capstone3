import { useEffect, useState } from "react";
import ItemCard from "../components/ItemCard";
import ItemList from "../components/ItemList";
import { Container, Card, Row, Col, Button } from "react-bootstrap";
import Swal from "sweetalert2";

import "../assets/css/index.css";

//env
const renderUrl = process.env.REACT_APP_RENDER_URL;
const localUrl = process.env.REACT_APP_LOCAL_URL;
const mode = process.env.REACT_APP_MODE;
const link = mode === "DEV" ? localUrl : renderUrl;
let token = localStorage.getItem("token");

export default function Supplies() {
  const [item, setItems] = useState([]);
  const [receipt, setReceipt] = useState([]);
  const [refresh, setRefresh] = useState(0);
  const [cart, setCart] = useState("");

  function refreshData() {
    setRefresh((x) => x + 1);
  }

  function filterData(items) {
    const result = [];

    // Group the items by name and sum the total quantity and price of each item
    const nameQuantityPriceMap = items.reduce((map, item) => {
      const itemName = item.itemName;
      const itemQuantity = item.quantity;
      const itemPrice = item.price;

      if (itemName in map) {
        map[itemName].quantity += itemQuantity;
        map[itemName].price += itemPrice;
      } else {
        map[itemName] = {
          quantity: itemQuantity,
          price: itemPrice,
        };
      }

      return map;
    }, {});

    // Convert the name-quantity-price map to an array of name-quantity-price objects
    Object.keys(nameQuantityPriceMap).forEach((name) => {
      result.push({
        itemName: name,
        quantity: nameQuantityPriceMap[name].quantity,
        price: nameQuantityPriceMap[name].price,
      });
    });
    return result;
  }

  function checkOut() {
    fetch(`${link}/api/cartDetails`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        saveOrder(data.data);
      });

    const saveOrder = (data) => {
      fetch(`${link}/api/checkout`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          data,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          if (data.status === "success") {
            clearCart();
            refreshData();
          }
        });
    };
  }

  function clearCart() {
    fetch(`${link}/api/cartDelete`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        refreshData();
        if (typeof data !== "undefined") {
          Swal.fire({
            title: "Order successful",
            icon: "success",
            text: "Awesome!",
          });

          setCart("No items");
          refreshData();
        } else {
          Swal.fire({
            title: "Order Failed",
            icon: "error",
            text: "Contact admin.",
          });
        }
      });
  }

  useEffect(() => {
    fetch(`${link}/api/availableSupplies`)
      .then((res) => res.json())
      .then((data) => {
        setItems(
          data.data.map((item) => {
            return (
              <ItemCard className="supply-cards" key={item._id} item={item} />
            );
          })
        );
      });

    fetch(`${link}/api/cartDetails`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        let x = filterData(data.data);
        setCart(
          x.map((item) => {
            return <ItemList key={item.id} item={item} />;
          })
        );
      });
  }, [refresh]);

  return (
    <Container fluid className="p-5 bg-white bg-main">
      <div className="bg">
        <div className="overlayBg">
          <Row className="p-5">
            <Col lg={9}>
              <h1 className="title-text text-white display-2">Marketplace</h1>

              <Row>
                <Col lg={6}>
                  <p className="text-light title-text">
                    Scandinavia bork bork bork swords bjorn the chair horns sea
                    horns ikea. Bjorn the chair ur mom berserker, longship
                    terror swords longship viking longship skald tall blond
                    women bork bork bork Beowulf. Raiding boats berserker, lack
                    the table ur mom scandinavia bjorn the chair ikea swords
                    ikea axes viking axes bjorn the chair.
                  </p>
                </Col>
                <Col lg={6}></Col>
              </Row>
            </Col>
            <Col lg={3} className="ms-auto">
              <Card className="ms-auto cart-card pt-2 bg-dark text-white">
                <Card.Title className="px-3 py-1">
                  CART
                  <Button
                    size="sm"
                    className="btn btn-outline-secondary bg-transparent text-warning mx-2"
                    onClick={() => {
                      checkOut();
                    }}
                  >
                    checkout
                  </Button>
                </Card.Title>
                <Card.Body>{cart}</Card.Body>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
      <Container>
        <Row className="d-flex flex-wrap p-2 mt-2 align-items-center justify-content-center">
          <h1 className="title-text text-white display-5">Items</h1>
          <Col className="container">
            <Row className="container d-flex flex-wrap justify-content-center">
              {item}
            </Row>
          </Col>
        </Row>
      </Container>
    </Container>
  );
}
