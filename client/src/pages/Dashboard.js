import {
  Button,
  Card,
  Col,
  Container,
  Row,
  Table,
  Form,
  Modal,
  ListGroup,
} from "react-bootstrap";
import "../assets/css/dashboard.css";

import { useEffect, useState } from "react";
const renderUrl = process.env.REACT_APP_RENDER_URL;
const localUrl = process.env.REACT_APP_LOCAL_URL;
const mode = process.env.REACT_APP_MODE;
const link = mode === "DEV" ? localUrl : renderUrl;

function Dashboard() {
  let token = localStorage.getItem("token");
  const [item, setItems] = useState([]);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => {
    setShow(true);
  };

  const [showUpdate, setShowUpdate] = useState(false);

  const handleCloseUpdate = () => setShowUpdate(false);
  const handleShowUpdate = () => {
    setShowUpdate(true);
  };

  function addStock(id, value) {
    if (window.confirm("Confirm to add stock")) {
      // console.log(token);
      if (id !== "" && value !== "") {
        fetch(`${link}/api/addSupplyStock`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            id: id,
            add: value,
          }),
        })
          .then((res) => {
            return res.json();
          })
          .then((data) => {
            // console.log(data);
            // setShowUpdate(false);
            document.getElementById(id + "input").value = "";
            refreshData();
          });
      } else {
        alert("please check input");
      }
    } else {
      console.log("cancelled add stock");
    }
  }

  function viewAllSupplies() {
    fetch(`${link}/api/supplies`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        // console.log(data);
        setviewAllProducts(
          data.data.map((item) => {
            let statusString = item.isActive.toString();
            return (
              <>
                <ListGroup>
                  <ListGroup.Item variant="dark">
                    Name: {item.name} Stock: {item.stock}
                    Category:{item.category}
                    Price:{item.price} Active : {statusString}
                  </ListGroup.Item>
                </ListGroup>
              </>
            );
          })
        );
      });
  }

  const [currentName, setcurrentName] = useState([]);
  const [currentStock, setcurrentStock] = useState([]);
  const [currentCategory, setcurrentCategory] = useState([]);
  const [currentPrice, setcurrentPrice] = useState([]);
  const [currentSupplyId, setcurrentSupplyId] = useState([]);

  const [updateName, setupdateName] = useState([]);
  const [updateStock, setupdateStock] = useState([]);
  const [updateCategory, setupdateCategory] = useState([]);
  const [updatePrice, setupdatePrice] = useState([]);

  const [addSupplyName, setaddSupplyName] = useState([]);
  const [addSupplyStock, setaddSupplyStock] = useState([]);
  const [addSupplyCategory, setaddSupplyCategory] = useState([]);
  const [addSupplyPrice, setaddSupplyPrice] = useState([]);

  const [viewAllProducts, setviewAllProducts] = useState([]);

  const [refresh, setRefresh] = useState(0);

  function refreshData() {
    setRefresh((x) => x + 1);
  }

  function updateModalData(data) {
    setcurrentName(data.name);
    setcurrentPrice(data.price);
    setcurrentStock(data.stock);
    setcurrentCategory(data.category);
    setcurrentSupplyId(data._id);

    setupdateName(data.name);
    setupdatePrice(data.price);
    setupdateStock(data.stock);
    setupdateCategory(data.category);

    handleShowUpdate();
  }

  function updateSupply(id) {
    console.log(updateStock);
    console.log(updateName);
    console.log(updateCategory);
    console.log(updatePrice);
    console.log("\x1b[33m update supply button clicked");

    fetch(`${link}/api/supply/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        name: updateName,
        stock: updateStock,
        category: updateCategory,
        price: updatePrice,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        // console.log(data);
        setShowUpdate(false);
        refreshData();
      });
  }

  function toggleStatus(id) {
    if (window.confirm("Toggle status?")) {
      fetch(`${link}/api/supply/toggleStatus/${id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          // console.log(data);
          refreshData();
        });
    } else {
      console.log("cancelled toggle status");
    }
    // console.log(id);
    console.log("\x1b[33m toggle status button clicked");
  }

  function addSupply() {
    if (
      addSupplyName !== "" &&
      addSupplyStock !== "" &&
      addSupplyCategory !== ""
    ) {
      try {
        fetch(link + "/api/addSupply", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            name: addSupplyName,
            stock: addSupplyStock,
            category: addSupplyCategory,
            price: addSupplyPrice,
            server: "default",
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            setShow(false);
            refreshData();
          })
          .catch((err) => {
            alert("supply name exist, or no input/s please try again");
          });
      } catch (error) {}
    } else {
      console.log("variable/s null");
    }

    console.log("\x1b[33m add supply button clicked");
    // setShow(false);
  }

  useEffect(() => {
    fetch(`${link}/api/supplies`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        // console.log(data);
        setItems(
          data.data.map((item) => {
            let statusString = item.isActive.toString();
            return (
              <tr>
                <th>{item.name}</th>
                <th>{item.stock}</th>
                <th>{item.category}</th>
                <th>{item.price}</th>
                <th>{statusString}</th>
                <th>
                  <input
                    id={item._id + "input"}
                    type="number"
                    step="any"
                    size="sm"
                    min="0"
                  />
                  <button
                    className="btn btn-outline-secondary m-1"
                    onClick={() => {
                      let x = document.getElementById(item._id + "input");
                      addStock(item._id, x.value);
                    }}
                  >
                    add
                  </button>
                  <button
                    className="btn btn-outline-secondary m-1"
                    onClick={() => {
                      updateModalData(item);
                    }}
                  >
                    update
                  </button>
                  <button
                    className="btn btn-outline-secondary m-1"
                    onClick={() => {
                      toggleStatus(item._id);
                    }}
                  >
                    toggle status
                  </button>
                </th>
              </tr>
            );
          })
        );
      });
  }, [refresh]);
  return (
    <Container className="p-5">
      <Row>
        <Col md={12} className="p-2">
          <Card className="bg-custom">
            <Row>
              <Col md={2}>
                <h3>Supplies</h3>
              </Col>
              <Col md={3}>
                <Button
                  size="sm"
                  className="btn btn-dark"
                  onClick={() => {
                    handleShow();
                  }}
                >
                  Add new supply
                </Button>
              </Col>
              <Col md={3} className="ms-auto">
                <Form.Select aria-label="Default select example">
                  <option>Select Server</option>
                  <option value="1">Lupon</option>
                  <option value="2">Pinoy</option>
                  <option value="3">Godro</option>
                  <option value="4">Aero</option>
                </Form.Select>
              </Col>
            </Row>

            <Table striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Stock</th>
                  <th>Category</th>
                  <th>Price</th>
                  <th>Active</th>
                  <th>Add stock</th>
                </tr>
              </thead>
              <tbody>{item}</tbody>
            </Table>
          </Card>
        </Col>
        <Col md={12} className="p-2">
          <Card className="bg-custom">
            {viewAllProducts}
            <Button
              variant="dark"
              onClick={() => {
                viewAllSupplies();
              }}
            >
              View all supplies
            </Button>
          </Card>
        </Col>
        <Col md={6} className="p-2">
          <Card className="bg-custom">
            <strong>War of Emperium Schedule</strong>
          </Card>
        </Col>
        <Col md={6} className="p-2">
          <Card className="bg-custom">
            <strong>Attendance</strong>
          </Card>
        </Col>
      </Row>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add Supply</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Label htmlFor="name">name</Form.Label>
          <Form.Control
            required
            type="text"
            id="name"
            placeholder="add name"
            onChange={(e) => {
              setaddSupplyName(e.target.value);
            }}
          />
          <Form.Label htmlFor="category">category</Form.Label>
          <Form.Control
            required
            type="text"
            id="category"
            placeholder="add category"
            onChange={(e) => {
              setaddSupplyCategory(e.target.value);
            }}
          />
          <Form.Label htmlFor="price">price</Form.Label>
          <Form.Control
            required
            type="text"
            id="price"
            placeholder="add price"
            onChange={(e) => {
              setaddSupplyPrice(e.target.value);
            }}
          />
          <Form.Label htmlFor="stock">stock</Form.Label>
          <Form.Control
            required
            type="text"
            id="stock"
            placeholder="add stock"
            onChange={(e) => {
              setaddSupplyStock(e.target.value);
            }}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            type="submit"
            variant="dark"
            onClick={() => {
              addSupply();
            }}
          >
            Add
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal show={showUpdate} onHide={handleCloseUpdate}>
        <Modal.Header closeButton>
          <Modal.Title>Update Supply</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Label htmlFor="name">name</Form.Label>
          <Form.Control
            type="text"
            id="name"
            defaultValue={currentName}
            onChange={(e) => {
              setupdateName(e.target.value);
            }}
          />
          <Form.Label htmlFor="category">category</Form.Label>
          <Form.Control
            type="text"
            id="category"
            defaultValue={currentCategory}
            onChange={(e) => {
              setupdateCategory(e.target.value);
            }}
          />
          <Form.Label htmlFor="price">price</Form.Label>
          <Form.Control
            type="text"
            id="price"
            defaultValue={currentPrice}
            onChange={(e) => {
              setupdatePrice(e.target.value);
            }}
          />
          <Form.Label htmlFor="stock">stock</Form.Label>
          <Form.Control
            type="text"
            id="stock"
            defaultValue={currentStock}
            onChange={(e) => {
              setupdateStock(e.target.value);
            }}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseUpdate}>
            Close
          </Button>
          <Button
            variant="dark"
            onClick={() => {
              updateSupply(currentSupplyId);
            }}
          >
            Update
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

export default Dashboard;
