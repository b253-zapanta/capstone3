//imports
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import { Navigate } from "react-router-dom";

import Swal from "sweetalert2";
import { useState, useEffect, useContext } from "react";

import UserContext from "../global/UserContex";
// import { getItemFromStorage } from "../global/useLocalStorage";

//env
const renderUrl = process.env.REACT_APP_RENDER_URL;
const localUrl = process.env.REACT_APP_LOCAL_URL;
const mode = process.env.REACT_APP_MODE;
const link = mode === "DEV" ? localUrl : renderUrl;

function Login() {
  // let isUser = getItemFromStorage("token");
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  console.log(link);
  console.log("link is above");

  function authenticate(e) {
    e.preventDefault();

    fetch(link + "/api/loginUser", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        if (typeof data.access !== "undefined") {
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome back",
          });
        } else {
          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Check your login credentials and try again.",
          });
        }
      });

    setEmail("");
    setPassword("");
  }

  const retrieveUserDetails = (token) => {
    fetch(link + "/api/userDetails", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        setUser({
          id: data.data._id,
          isAdmin: data.data.isAdmin,
        });
      });
  };

  useEffect(() => {
    // console.log(user.id);
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password, user.id]);

  return user.id !== null ? (
    <Navigate to="/" />
  ) : (
    <Container>
      <h1 className="my-5 display-3">Login</h1>
      <Form className="mt-5" onSubmit={(e) => authenticate(e)}>
        <Form.Group className="mb-3">
          <Row className="mb-2">
            <Col sm={4}>
              <Form.Label>Email address</Form.Label>
              <Form.Control
                size="sm"
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="user@mail.com"
              />
            </Col>
          </Row>
        </Form.Group>
        <Form.Group className="mb-4">
          <Row className="my-2">
            <Col sm={4}>
              <Form.Label>Password</Form.Label>
              <Form.Control
                size="sm"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder="minimum of 8 characters"
              />
            </Col>
          </Row>
        </Form.Group>
        {isActive ? (
          <Button variant="dark" type="submit" id="submitBtn">
            Login
          </Button>
        ) : (
          <Button variant="secondary" type="submit" id="submitBtn" disabled>
            Login
          </Button>
        )}
      </Form>
    </Container>
  );
}

export default Login;
