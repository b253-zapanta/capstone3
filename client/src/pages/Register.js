import { useState, useEffect, useContext } from "react";
import { Form, Button, Row, Col, Container } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import UserContext from "../global/UserContex";
import Swal from "sweetalert2";

// env
const renderUrl = process.env.REACT_APP_RENDER_URL;
const localUrl = process.env.REACT_APP_LOCAL_URL;
const mode = process.env.REACT_APP_MODE;
const link = mode === "DEV" ? localUrl : renderUrl;

export default function Register() {
  const { user } = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  const [registerSuccess, setRegisterSuccess] = useState(false);

  function registerUser(e) {
    e.preventDefault();

    // console.log(email);
    fetch(link + "/api/checkEmail", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Email already exists.",
          });
        } else {
          fetch(link + "/api/createUser", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNo,
              password: password1,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              // console.log(data);
              if (data) {
                Swal.fire({
                  title: "Registration Successful",
                  icon: "success",
                  text: "Welcome to our guild!",
                });
                setRegisterSuccess(true);
              } else {
                Swal.fire({
                  title: "Registration Failed",
                  icon: "error",
                  text: "Please contact us for this issue!",
                });
              }
            });
        }
      });
  }

  useEffect(() => {
    console.log(user.isAdmin, "isadmin test here");
    if (
      firstName !== "" &&
      lastName !== "" &&
      mobileNo !== "" &&
      email !== "" &&
      password1 !== "" &&
      mobileNo.length >= 11 &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [
    email,
    password1,
    password2,
    firstName,
    lastName,
    mobileNo,
    user.isAdmin,
  ]);

  return registerSuccess !== false ? (
    <Navigate to="/login" />
  ) : (
    <Container className="mt-5">
      <h1 className="mb-5 display-3">Register</h1>
      <Form onSubmit={(e) => registerUser(e)}>
        <Form.Group className="mb-3" controlId="userFirstName">
          <Row className="mb-2">
            <Col lg={4}>
              <Form.Label>First Name</Form.Label>
              <Form.Control
                size="sm"
                type="string"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                placeholder="Enter first name"
                required
              />
            </Col>
          </Row>
        </Form.Group>

        <Form.Group className="mb-3" controlId="userLastName">
          <Row className="mb-2">
            <Col lg={4}>
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                size="sm"
                type="string"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                placeholder="Enter last name"
                required
              />
            </Col>
          </Row>
        </Form.Group>

        <Form.Group className="mb-3" controlId="userEmail">
          <Row className="mb-2">
            <Col lg={4}>
              <Form.Label>Email</Form.Label>
              <Form.Control
                size="sm"
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="Enter email"
                required
              />
            </Col>
          </Row>
        </Form.Group>

        <Form.Group className="mb-3" controlId="userMobileNo">
          <Row className="mb-2">
            <Col lg={4}>
              <Form.Label>Mobile Number</Form.Label>
              <Form.Control
                size="sm"
                type="string"
                value={mobileNo}
                onChange={(e) => setMobileNo(e.target.value)}
                placeholder="Enter mobile no"
                required
              />
            </Col>
          </Row>
        </Form.Group>

        <Form.Group className="mb-3" controlId="password1">
          <Row className="mb-2">
            <Col lg={4}>
              <Form.Label>Password</Form.Label>
              <Form.Control
                size="sm"
                type="password"
                value={password1}
                onChange={(e) => setPassword1(e.target.value)}
                placeholder="Enter password"
                required
              />
            </Col>
          </Row>
        </Form.Group>

        <Form.Group className="mb-3" controlId="password2">
          <Row className="mb-2">
            <Col lg={4}>
              <Form.Label>Confirm</Form.Label>
              <Form.Control
                size="sm"
                type="password"
                value={password2}
                onChange={(e) => setPassword2(e.target.value)}
                placeholder="Confirm password"
                required
              />
            </Col>
          </Row>
        </Form.Group>

        {isActive ? (
          <Button variant="dark" size="sm" type="submit" id="submitBtn">
            Submit
          </Button>
        ) : (
          <Button
            variant="danger"
            size="sm"
            type="submit"
            id="submitBtn"
            disabled
          >
            Submit
          </Button>
        )}
      </Form>
    </Container>
  );
}
