import { useEffect, useState } from "react";
import { Container, Card, Row, Col, Button } from "react-bootstrap";
import OrderList from "../components/OrderList";

export default function MyOrder() {
  //env
  const renderUrl = process.env.REACT_APP_RENDER_URL;
  const localUrl = process.env.REACT_APP_LOCAL_URL;
  const mode = process.env.REACT_APP_MODE;
  const link = mode === "DEV" ? localUrl : renderUrl;
  let token = localStorage.getItem("token");

  const [order, setOrder] = useState([]);
  const [refresh, setRefresh] = useState(0);

  const [test, setTest] = useState([]);

  function refreshData() {
    setRefresh((x) => x + 1);
  }

  useEffect(() => {
    fetch(`${link}/api/orderDetails`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        // let x = [data];
        setOrder(
          data.map((items) => {
            return (
              <OrderList
                className="supply-cards"
                key={items._id}
                item={items}
              />
            );
          })
        );
      });
  }, [refresh]);
  return (
    <Container>
      <Card>
        <Card.Text>Orders</Card.Text>
        <Card.Text>{order}</Card.Text>
      </Card>
      {/* <Button
        onClick={() => {
          setRefresh((x) => x + 1);
        }}
      >
        asd
      </Button> */}
    </Container>
  );
}
