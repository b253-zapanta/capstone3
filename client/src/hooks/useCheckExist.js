import { useState, useEffect } from "react";

const useCheckExist = (field, value, api) => {
  // console.log(field, value, api);
  const [result, setResult] = useState();
  const [error, setError] = useState();

  useEffect(() => {
    fetch("http://localhost:4000/api/checkEmail", {
      method: "POST",
      body: JSON.stringify({
        email: "personone@mail",
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data, "log data");
        setResult(data);
      })
      .catch((err) => {
        setError(err.message);
      });
  }, [field, value, api]);

  return { result, error };
};

export default useCheckExist;
