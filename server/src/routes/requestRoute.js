const express = require("express");
const router = express.Router();
const requestController = require("../controllers/requestController");

router.post("/request", requestController.request_supplies);
router.get("/requestDetails", requestController.request_details);

module.exports = router;
