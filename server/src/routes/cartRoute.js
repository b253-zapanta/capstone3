const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartController");

router.post("/addToCart", cartController.cart_add);
router.get("/cartDetails", cartController.cart_get);
router.get("/cartDelete", cartController.cart_delete);

module.exports = router;
