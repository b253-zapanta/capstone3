const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

// productController
router.get("/products", productController.product_display);
router.get("/activeProducts", productController.product_active);
router.get("/activeProducts", productController.product_active);
router.post("/createProduct", productController.product_create);
router.put("/products/:id/archive", productController.product_archive);
router.put("/products/:id/activate", productController.product_activate);
router
  .route("/products/:id")
  .get(productController.product_get)
  .patch(productController.product_patch)
  .put(productController.product_update);
// .delete(productController.user_delete)
// .get(productController.user_details);

module.exports = router;
