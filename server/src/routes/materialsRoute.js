const express = require("express");
const router = express.Router();
const materialsController = require("../controllers/materialsController");

router.get("/materials", materialsController.materials_display);
router.get("/material/:id", materialsController.materials_details);
router.get("/availableMaterials", materialsController.materials_available);
router.post("/addMaterails", materialsController.materials_create);
router.put("/addSupplyStock", materialsController.materials_stock);

module.exports = router;
