const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");

// productController
router.get("/orders", orderController.order_display);
router.get("/orderDetails", orderController.order_details);
router.post("/orderCheckout", orderController.order_checkout);
router.post("/checkout", orderController.checkout_order);

module.exports = router;
