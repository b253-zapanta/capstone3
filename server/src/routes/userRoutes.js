const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");

// userController
router.get("/users", userController.users_display);
router.post("/checkEmail", userController.users_check_email);
router.get("/userDetails", userController.user_profile);
router.post("/createUser", userController.user_create);
router.post("/loginUser", userController.user_login);
router.post("/:id/setUserAsAdmin", userController.user_set_admin);
router.route;
//   .patch(userController.user_update)
//   .put(userController.user_update)
//   .delete(userController.user_delete)
//   .get(userController.user_details);

module.exports = router;
