const express = require("express");
const router = express.Router();
const suppliesController = require("../controllers/suppliesController");

router.get("/supplies", suppliesController.supplies_display);
router.get("/supply/:id", suppliesController.supplies_details);
router.get("/availableSupplies", suppliesController.supplies_available);
router.post("/addSupply", suppliesController.supplies_create);
router.put("/addSupplyStock", suppliesController.supplies_stock);
router.put("/supply/archive", suppliesController.supplies_archive);
router.put("/supply/activate", suppliesController.supplies_activate);
router.put(
  "/supply/toggleStatus/:id",
  suppliesController.supplies_toggle_status
);

router
  .route("/supply/:id")
  .patch(suppliesController.supply_patch)
  .put(suppliesController.supply_update);

module.exports = router;
