const mongoose = require("mongoose");

const requestSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "id is required"],
  },
  server: {
    type: String,
    required: [true, "firstName is required"],
  },
  requestedOn: {
    type: Date,
    default: new Date(),
  },
  status: {
    type: String,
    default: "not approved",
    // shipped completed cancelled
  },
  request: [
    {
      itemId: { type: String },
      quantity: { type: Number },
    },
  ],
});

module.exports = mongoose.model("Request", requestSchema);
