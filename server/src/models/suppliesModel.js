const mongoose = require("mongoose");

const suppliesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true],
  },
  category: {
    type: String,
    required: [true],
  },
  stock: {
    type: Number,
    required: [true],
  },
  price: {
    type: Number,
    required: [true],
  },
  server: {
    type: String,
    required: [true],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Supplies", suppliesSchema);
