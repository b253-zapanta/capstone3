const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First name is required"],
  },
  lastName: {
    type: String,
    required: [true, "First name is required"],
  },
  email: {
    type: String,
    required: [true, "Email is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  mobileNo: {
    type: String,
    default: "00000000000",
  },
  role: [
    {
      job: {
        type: String,
        default: "",
      },
      server: {
        type: String,
        default: "",
      },
    },
  ],
  supplies: [
    {
      supplyName: {
        type: String,
      },
      quantity: {
        type: Number,
        default: 0,
      },
    },
  ],
  materials: [
    {
      materialName: {
        type: String,
      },
      quantity: {
        type: Number,
        default: 0,
      },
    },
  ],
});

module.exports = mongoose.model("User", userSchema);
