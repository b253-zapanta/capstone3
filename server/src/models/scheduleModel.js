const mongoose = require("mongoose");

const scheduleSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true],
  },
  type: {
    type: String,
    required: [true],
  },
  server: {
    type: String,
    required: [true],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Schedule", scheduleSchema);
