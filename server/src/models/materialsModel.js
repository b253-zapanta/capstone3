const mongoose = require("mongoose");

const materialsSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Product name is required"],
  },
  server: {
    type: String,
    required: [true],
  },
  category: {
    type: String,
    required: [true, "Product name is required"],
  },
  stock: {
    type: Number,
    required: [true, "Product name is required"],
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Materials", materialsSchema);
