const mongoose = require("mongoose");
const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
  },

  item: [
    {
      itemId: {
        type: String,
      },
      quantity: {
        type: Number,
      },
      price: {
        type: Number,
      },
      itemName: {
        type: String,
        required: [true],
      },
      addedOn: {
        type: Date,
        defaul: new Date(),
      },
    },
  ],
});

module.exports = mongoose.model("Cart", cartSchema);
