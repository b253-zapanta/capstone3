const Cart = require("../models/cartModel");
let currentUser = require("../global/currentUser");

const cart_add = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  try {
    let checkDuplicate = await Cart.findOne({
      userId: userDetails.id,
    });
    if (checkDuplicate) {
      try {
        getExisting = await Cart.findByIdAndUpdate(checkDuplicate._id, {
          $push: { item: request.body },
        });

        pushedItems = await Cart.findById(checkDuplicate._id);

        //update stock (subtract)
        const Supplies = require("../models/suppliesModel");
        const getItem = await Supplies.findById(request.body.itemId);
        let newStockValue = getItem.stock - request.body.quantity;

        let updateStock = await Supplies.findByIdAndUpdate(
          request.body.itemId,
          { stock: newStockValue }
        );

        const newData = await Supplies.findById(request.body.itemId);

        return response.status(201).json({ status: "success", data: newData });
      } catch (error) {
        console.log(error, "\n getExisting supplies_create error above");
        return response.status(422).send("add items in existing cart failed");
      }
    } else {
      let newItem = new Cart({
        userId: userDetails.id,
        item: [
          {
            itemId: request.body.itemId,
            quantity: request.body.quantity,
            itemName: request.body.itemName,
            price: request.body.price,
          },
        ],
      });
      await newItem.save();
      return response.status(201).json({
        status: "success",
        msg: "New cart added",
        data: newItem,
      });
    }
  } catch (error) {
    console.log(error, "\n cart_add error above");
    return response.status(400).send(error);
  }
};
const cart_get = async (request, response) => {
  const userDetails = await currentUser.current_user(request);

  try {
    const getCartDetails = await Cart.findOne({ userId: userDetails.id });

    if (getCartDetails)
      return response
        .status(201)
        .json({ status: "success", data: getCartDetails.item });
    return response.status(201).json({
      status: "success",
      data: "<span>No items inside the cart</span>",
    });
  } catch (error) {
    console.log(error, "\n supplies_display error above");
    return response.status(400).send(error);
  }
};

const cart_delete = async (request, response) => {
  const userDetails = await currentUser.current_user(request);

  console.log("asdpoajsdaksjdlakjd");

  try {
    const deleteCart = await Cart.findOneAndDelete({ userId: userDetails.id });
    return response.status(201).json({ status: "success", data: deleteCart });
  } catch (error) {
    console.log(error, "\n cart_delete error above");
    return response.status(400).send(error);
  }
};

module.exports = {
  cart_add,
  cart_get,
  cart_delete,
};
