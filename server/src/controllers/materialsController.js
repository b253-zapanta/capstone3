const Materials = require("../models/materialsModel");
let currentUser = require("../global/currentUser");

const materials_display = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  if (userDetails.isAdmin) {
    try {
      const getMaterials = await Materials.find();
      if (!getMaterials.length)
        return response.status(201).send("No materials at the moment");
      return response
        .status(201)
        .json({ status: "success", data: getMaterials });
    } catch (error) {
      console.log(error, "\n materials_display error above");
      return response.status(400).send(error);
    }
  } else {
    response.status(201).send("Only admin can view all products");
  }
};
const materials_details = async (request, response) => {
  try {
    const getMaterial = await Materials.findById(request.params.id);
    return response.status(200).send(getMaterial);
  } catch (error) {
    console.log(error, "\n materials_details error above");
    return response.status(400).send(error);
  }
};
const materials_available = async (request, response) => {
  try {
    const getMaterial = await Materials.find({ isActive: true });
    // console.log(getSupplies);
    if (!getMaterial) return response.status(201).send("No supply available");
    return response.status(201).json({ status: "success", data: getMaterial });
  } catch (error) {
    console.log(error, "\n materials_available error above");
    return response.status(400).send(error);
  }
};
const materials_create = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  if (userDetails.isAdmin) {
    try {
      checkDuplicate = await Materials.findOne({
        name: request.body.name,
      });
      if (!checkDuplicate) {
        try {
          let newMaterial = new Supplies({
            name: request.body.name,
            category: request.body.category,
            stock: request.body.stock,
            server: request.body.server,
          });
          await newMaterial.save();
          return response.status(201).json({
            status: "success",
            msg: "New material created",
            data: newMaterial,
          });
        } catch (error) {
          console.log(error, "\n supplies_create error above");
          return response.status(422).send("Create material failed");
        }
      } else {
        response.status(201).send("Material name exists");
      }
    } catch (error) {
      console.log(error, "\n materials_create error above");
      return response.status(400).send(error);
    }
  } else {
    response.status(201).send("Only admin can add new material");
  }
};
const materials_stock = async (request, response) => {
  return null;
};

module.exports = {
  materials_display,
  materials_details,
  materials_available,
  materials_create,
  materials_stock,
};
