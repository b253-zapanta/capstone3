const Products = require("../models/productModel");
let currentUser = require("../global/currentUser");

// Display All products
const product_display = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  if (userDetails.isAdmin) {
    try {
      const getProducts = await Products.find();
      if (!getProducts.length)
        return response.status(201).send("No products at the moment");
      return response
        .status(201)
        .json({ status: "success", data: getProducts });
    } catch (error) {
      console.log(error);
      return response.status(400).send(error);
    }
  } else {
    response.status(201).send("Only admin can view all products");
  }
};

// Create New product
const product_create = async (request, response) => {
  const userDetails = await currentUser.current_user(request);

  if (userDetails.isAdmin) {
    checkDuplicate = await Products.findOne({
      productName: request.body.productName,
    });
    if (!checkDuplicate) {
      try {
        let newProduct = new Products({
          productName: request.body.productName,
          productDescription: request.body.productDescription,
          stock: request.body.stock,
          price: request.body.price,
        });
        await newProduct.save();
        return response.status(201).json({
          status: "success",
          msg: "New product added",
          data: newProduct,
        });
      } catch (error) {
        console.log(error);
        return response.status(422).send("Add product failed");
      }
    } else {
      response.status(201).send("Product name exists");
    }
  } else {
    response.status(201).send("Only admin can add products");
  }
};

// active products
const product_active = async (request, response) => {
  try {
    const getProducts = await Products.find({ isActive: true });
    if (!getProducts.length)
      return response.status(201).send("No products at the moment");
    return response.status(201).json({ status: "success", data: getProducts });
  } catch (error) {
    console.log(error);
    return response.status(400).send(error);
  }
};
// get product by ID
const product_get = async (request, response) => {
  try {
    const getProduct = await Products.findById(request.params.id);
    if (!getProduct) return response.status(201).send("Product not exist");
    return response.status(201).json({ status: "success", data: getProduct });
  } catch (error) {
    return response.status(400).send(error);
  }
};

// patch product
const product_patch = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  let updateProduct = request.body;

  if (userDetails.isAdmin) {
    try {
      const product = await Products.findById(request.params.id);
      if (product) {
        const update = await Products.findByIdAndUpdate(
          request.params.id,
          updateProduct
        );
        const product = await Products.findById(request.params.id);
        return response
          .status(201)
          .json({ status: "Product updated!", data: product });
      } else {
        return response.status(201).json({ status: "Product does not exist!" });
      }
    } catch (error) {
      console.log("Failed to update product");
      return response.status(400).send(error);
    }
  } else {
    return response.status(201).send("Only admin can update a product");
  }
};

// update product
const product_update = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  let updateProduct = request.body;

  if (userDetails.isAdmin) {
    try {
      const product = await Products.findById(request.params.id);
      if (product) {
        const update = await Products.findByIdAndUpdate(
          request.params.id,
          updateProduct
        );

        const product = await Products.findById(request.params.id);
        return response
          .status(201)
          .json({ status: "Product updated!", data: product });
      } else {
        return response.status(201).json({ status: "Product does not exist!" });
      }
    } catch (error) {
      console.log("Failed to update product");
      return response.status(400).send(error);
    }
  } else {
    return response.status(201).send("Only admin can update a product");
  }
};

const product_archive = async (request, response) => {
  const userDetails = await currentUser.current_user(request);

  if (userDetails.isAdmin) {
    try {
      const product = await Products.findById(request.params.id);
      if (product) {
        const getProduct = await Products.findById(request.params.id);
        getProduct.isActive = false;
        return response
          .status(201)
          .json({ status: "Product archived!", data: getProduct });
      } else {
        return response.status(201).json({ status: "Product does not exist!" });
      }
    } catch (error) {
      console.log("Failed to archive product");
      return response.status(400).send(error);
    }
  } else {
    return response.status(201).send("Only admin can archive a product");
  }
};

const product_activate = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  if (userDetails.isAdmin) {
    try {
      const product = await Products.findById(request.params.id);
      if (product) {
        const getProduct = await Products.findById(request.params.id);
        getProduct.isActive = true;
        return response
          .status(201)
          .json({ status: "Product activated!", data: getProduct });
      } else {
        return response.status(201).json({ status: "Product does not exist!" });
      }
    } catch (error) {
      console.log("Failed to activate product");
      return response.status(400).send(error);
    }
  } else {
    return response.status(201).send("Only admin can activate a product");
  }
};

module.exports = {
  product_display,
  product_create,
  product_active,
  product_get,
  product_patch,
  product_archive,
  product_update,
  product_activate,
};
