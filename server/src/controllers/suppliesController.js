const Supplies = require("../models/suppliesModel");
let currentUser = require("../global/currentUser");

// Display All products
const supplies_display = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  if (userDetails.isAdmin) {
    try {
      const getSupplies = await Supplies.find();
      if (!getSupplies.length)
        return response.status(201).send("No supplies at the moment");
      return response
        .status(201)
        .json({ status: "success", data: getSupplies });
    } catch (error) {
      console.log(error, "\n supplies_display error above");
      return response.status(400).send(error);
    }
  } else {
    response.status(201).send("Only admin can view all supplies");
  }
};

const supplies_available = async (request, response) => {
  try {
    const getSupplies = await Supplies.find({
      isActive: true,
      stock: { $ne: 0 },
    });
    if (!getSupplies) return response.status(201).send("No supply available");
    return response.status(201).json({ status: "success", data: getSupplies });
  } catch (error) {
    console.log(error, "\n supplies_available error above");
    return response.status(400).send(error);
  }
};
const supplies_create = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  if (userDetails.isAdmin) {
    try {
      checkDuplicate = await Supplies.findOne({
        name: request.body.name,
      });
      if (!checkDuplicate) {
        try {
          let newSupply = new Supplies({
            name: request.body.name,
            category: request.body.category,
            stock: request.body.stock,
            server: request.body.server,
            price: request.body.price,
          });
          await newSupply.save();
          return response.status(201).json({
            status: "success",
            msg: "New supply created",
            data: newSupply,
          });
        } catch (error) {
          console.log(error, "\n supplies_create error above");
          return response.status(422).send("Create supply failed");
        }
      } else {
        response.status(201).send("Supply name exists");
      }
    } catch (error) {
      console.log(error, "\n supplies_create error above");
      return response.status(400).send(error);
    }
  } else {
    response.status(201).send("Only admin can add new supply");
  }
};

const supplies_stock = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  if (userDetails.isAdmin) {
    console.log("user is admin");
    try {
      const getSupplies = await Supplies.findById(request.body.id);
      if (getSupplies) {
        x = parseInt(getSupplies.stock);
        y = parseInt(request.body.add);
        z = x + y;
        const updateStock = await Supplies.findByIdAndUpdate(request.body.id, {
          stock: parseInt(z),
        });
      }
      // getSupplies.stock += request.body.add;
      // getSupplies.save();
      return response.status(200).send(true);
    } catch (error) {
      console.log(error, "\n supplies_stock error above");
      return response.status(400).send(error);
    }
  } else {
    response.status(201).send("Only admin can add");
  }
};

const supplies_archive = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  if (userDetails.isAdmin) {
    try {
      const getSupplies = await Supplies.findById(request.body.id);
      getSupplies.isActive = false;
      getSupplies.save();
      return response.status(200).send(true);
    } catch (error) {
      console.log(error, "\n supplies_archive error above");
      return response.status(400).send(error);
    }
  } else {
    response.status(201).send("Only admin can archive supplies");
  }
};

const supplies_toggle_status = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  if (userDetails.isAdmin) {
    try {
      const getSupplies = await Supplies.findById(request.params.id);
      if (getSupplies.isActive) {
        const updateSupply = await Supplies.findByIdAndUpdate(
          request.params.id,
          { isActive: false }
        );
      } else {
        const updateSupply = await Supplies.findByIdAndUpdate(
          request.params.id,
          { isActive: true }
        );
      }

      const showResult = await Supplies.findById(request.params.id);
      return response
        .status(200)
        .json({ status: "Supply updated!", data: showResult });
    } catch (error) {
      console.log(error, "\n supplies_archive error above");
      return response.status(400).send(error);
    }
  } else {
    response.status(201).send("Only admin can archive supplies");
  }
};

const supplies_activate = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  if (userDetails.isAdmin) {
    try {
      const getSupplies = await Supplies.findById(request.body.id);
      getSupplies.isActive = true;
      getSupplies.save();
      return response.status(200).send(true);
    } catch (error) {
      console.log(error, "\n supplies_archive error above");
      return response.status(400).send(error);
    }
  } else {
    response.status(201).send("Only admin can archive supplies");
  }
};

const supplies_details = async (request, response) => {
  try {
    const getSupplies = await Supplies.findById(request.params.id);
    return response.status(200).send(getSupplies);
  } catch (error) {
    console.log(error, "\n supplies_details error above");
    return response.status(400).send(error);
  }
};

// patch product
const supply_patch = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  let updateSupply = request.body;

  if (userDetails.isAdmin) {
    try {
      const product = await Supplies.findById(request.params.id);
      if (product) {
        const update = await Supplies.findByIdAndUpdate(
          request.params.id,
          updateSupply
        );
        const supply = await Supplies.findById(request.params.id);
        return response
          .status(201)
          .json({ status: "Supply updated!", data: supply });
      } else {
        return response.status(201).json({ status: "Supply does not exist!" });
      }
    } catch (error) {
      console.log("Failed to update supply");
      return response.status(400).send(error);
    }
  } else {
    return response.status(201).send("Only admin can update a supply");
  }
};

// update product
const supply_update = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  let updateProduct = request.body;

  if (userDetails.isAdmin) {
    try {
      const product = await Products.findById(request.params.id);
      if (product) {
        const update = await Products.findByIdAndUpdate(
          request.params.id,
          updateProduct
        );

        const product = await Products.findById(request.params.id);
        return response
          .status(201)
          .json({ status: "Product updated!", data: product });
      } else {
        return response.status(201).json({ status: "Product does not exist!" });
      }
    } catch (error) {
      console.log("Failed to update product");
      return response.status(400).send(error);
    }
  } else {
    return response.status(201).send("Only admin can update a product");
  }
};

module.exports = {
  supplies_display,
  supplies_available,
  supplies_create,
  supplies_stock,
  supplies_archive,
  supplies_activate,
  supplies_details,
  supply_patch,
  supply_update,
  supplies_toggle_status,
};
