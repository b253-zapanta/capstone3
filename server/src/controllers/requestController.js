const Request = require("../models/requestModel");
const Supplies = require("../models/suppliesModel");
let currentUser = require("../global/currentUser");

// Display All products
const request_supplies = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  try {
    getStock = await Supplies.findById(request.body.itemId);
    let newStockValue = getStock.stock - request.body.quantity;
    updateStock = await Supplies.findByIdAndUpdate(request.body.itemId, {
      stock: newStockValue,
    });

    existingRequest = await Request.findOne({
      userId: userDetails.id,
    });

    newItem = {
      itemId: request.body.itemId,
      quantity: request.body.quantity,
    };

    if (existingRequest) {
      x = pushNewItem = await Request.findByIdAndUpdate(existingRequest._id, {
        $push: { request: newItem },
      });

      return response.status(201).json({
        status: "success",
        msg: "request updated",
      });
    } else {
      let newRequest = new Request({
        userId: userDetails.id,
        server: "test",
        request: [
          {
            itemId: request.body.itemId,
            quantity: request.body.quantity,
          },
        ],
      });
      await newRequest.save();
      return response.status(201).json({
        status: "success",
        msg: "New request added",
        data: newRequest,
      });
    }
  } catch (error) {
    console.log(error, "request_supplies error above");
    return response.status(400).send(error);
  }
};

const request_details = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  // // console.log(userDetails);
  try {
    const getRequest = await Request.findOne({ userId: userDetails.id });

    let requestedItemData = [];

    for (let x = 0; x < getRequest.request.length; x++) {
      const getItemIds = await Request.findOne({ userId: userDetails.id });
      const getDetails = await Supplies.findById(getItemIds.request[x].itemId);

      requestedItemData.push({
        name: getDetails.name,
        quantity: getItemIds.request[x].quantity,
      });
    }

    return response
      .status(201)
      .json({ status: "success", data: requestedItemData });
  } catch (error) {
    console.log(error, "\n supplies_display error above");
    return response.status(400).send(error);
  }
};

module.exports = {
  request_supplies,
  request_details,
};
