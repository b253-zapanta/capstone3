const Orders = require("../models/orderModel");
let currentUser = require("../global/currentUser");

// Display All users Data
const order_display = async (request, response) => {
  try {
    const getOrder = await Orders.find();
    if (!getOrder.length)
      return response.status(201).send("No OrderDetails yet");
    return response.status(201).json({ status: "success", data: getOrder });
  } catch (error) {
    console.log(error);
    return response.status(400).send(error);
  }
};

const order_details = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  // console.log(userDetails);
  try {
    // const getOrderDetails = await Orders.find();
    const getOrderDetails = await Orders.find({
      userId: userDetails.id,
    });
    // console.log(getOrderDetails);
    return response.status(200).json(getOrderDetails);
  } catch (error) {
    return response.status(400).send(error);
  }
};

const order_checkout = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  const Products = require("../models/productModel");
  if (!userDetails.isAdmin) {
    try {
      let total = 0;
      let newOrder = new Orders({
        userId: request.body.userId,
        totalAmount: 0,
        products: [],
      });
      for (let x = 0; x < request.body.products.length; x++) {
        const getProducts = await Products.findById(
          request.body.products[x].productId
        );
        total += getProducts.price * request.body.products[x].quantity;
        newOrder.products.push({
          productId: request.body.products[x].productId,
          quantity: request.body.products[x].quantity,
        });

        newOrder.userId = request.body.userId;
        newOrder.totalAmount = total;
      }

      await newOrder.save();
      return response.status(201).json({
        status: "success",
        msg: "New order added",
        data: newOrder,
      });
    } catch (error) {
      console.log(error);
      return response.status(422).send("Add order failed");
    }
  } else {
    return response.status(201).json({ status: "Current user is admin" });
  }
};

const user_orders = async (request, response) => {
  const userDetails = await currentUser.current_user(request);
  if (userDetails.id == request.params.id) {
    try {
      const orders = await Orders.find({ userId: request.params.id });
      return response.status(201).json({ status: "success", data: orders });
    } catch (error) {}
  } else {
    return response.status(201).send("no user login");
  }
};

const checkout_order = async (request, response) => {
  request.body.data;
  const userDetails = await currentUser.current_user(request);

  try {
    let total = 0;
    for (let index = 0; index < request.body.data.length; index++) {
      total +=
        request.body.data[index].price * request.body.data[index].quantity;
    }

    let newOrder = new Orders({
      userId: userDetails.id,
      items: request.body.data,
      totalAmount: total,
    });

    await newOrder.save();
    return response.status(201).json({ status: "success", data: newOrder });
  } catch (error) {
    console.log(error);
    return response.status(201).send(error);
  }
};

module.exports = {
  order_display,
  order_details,
  order_checkout,
  user_orders,
  checkout_order,
};
