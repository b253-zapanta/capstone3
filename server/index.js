const dotenv = require("dotenv").config();
const express = require("express");
const cors = require("cors");
const connection = require("./db");
const app = express();
const PORT = process.env.PORT || 4000;

/* ====================== Database connection ==========================
----------------------------------------------------------------------*/
connection(); //db.js
/*---------------------------------------------------------------------
=====================================================================*/

/* ====================== Middleware ==================================
---------------------------------------------------------------------*/
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.use((req, res, next) => {
  res.locals.path = req.path;
  next();
});
/*--------------------------------------------------------------------
===================================================================*/
/* ====================== Listen port ===================================
----------------------------------------------------------------------*/
app.listen(PORT, () => console.log(`Listening on port ${PORT}...`));
/*--------------------------------------------------------------------
===================================================================*/

/* ======================= Routes ====================================
-------------------------------------------------------------------*/
//routes path
const userRoutes = require("./src/routes/userRoutes");
const suppliesRoute = require("./src/routes/suppliesRoute");
const requestRoute = require("./src/routes/requestRoute");
const materialsRoute = require("./src/routes/materialsRoute");
const cartRoute = require("./src/routes/cartRoute");
const orderRoute = require("./src/routes/orderRoutes");

// routes
app.use("/api", userRoutes);
app.use("/api", suppliesRoute);
app.use("/api", requestRoute);
app.use("/api", materialsRoute);
app.use("/api", cartRoute);
app.use("/api", orderRoute);

//app.use("/api/auth", authRoute);
/*-------------------------------------------------------------------
==================================================================*/
